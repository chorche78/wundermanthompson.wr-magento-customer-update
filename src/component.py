"""
Template Component main class.

"""
import csv
import json
import logging

from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException
from keboola.http_client import HttpClient

# configuration variables
KEY_API_TOKEN = '#api_token'
KEY_URL = 'baseurl'
KEY_USER = 'username'
KEY_PASS = '#password'


# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = [KEY_URL, KEY_USER, KEY_PASS]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):

    def __init__(self):
        super().__init__()

    def run(self):
        """
        Main execution code
        """

        # check for missing configuration parameters
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.validate_image_parameters(REQUIRED_IMAGE_PARS)
        params = self.configuration.parameters

        def get_info(magento_id: str):
            """
            get info about customer
            check whether id of customer exists in magento
            if not, nothing will happen and row goes to log
            if exists put method for update existing customer and endpoint is set
            """

            endpoint_get = 'rest/default/V1/customers/' + magento_id
            response_get = cl_work.get_raw(endpoint_get)
            out = response_get.json()
            if 'message' in out.keys():
                return False, '', ''
            else:
                return True, 'rest/default/V1/customers/' + magento_id, 'put'

        csvlt = '\n'
        csvdel = ','
        csvquo = '"'

        # credentials
        username = params['username']
        password = params['#password']
        url_hostname = params['baseurl']
        output_log = False

        tables = self.get_input_tables_definitions()
        try:
            input_tables = tables[0]
            infile = input_tables.full_path
            logging.info(infile)
        except IndexError:
            logging.warning('no input table')
            exit(1)

        out_table = self.create_out_table_definition('DeviceChange_log.csv')
        outfile = out_table.full_path
        logging.info(outfile)

        # check for empty file, if empty stop the code
        with open(infile, 'r') as inp:
            inp.readline()
            lin = inp.readline()
            has_data = len(lin)

        if not has_data:
            logging.warning('input file -  no data')
            exit(0)

        # generate the access token and set the Header
        data = {"username": username, "password": password}
        cl_token = HttpClient(url_hostname)
        response = cl_token.post('rest/V1/integration/admin/token', data=json.dumps(data),
                                 headers={'Content-Type': 'application/json'})
        auth_token = 'Bearer {}'.format(response)
        logging.info('token created')

        auth_header = {'Authorization': auth_token}
        default_header = {'Content-Type': 'application/json'}
        cl_work = HttpClient(url_hostname, default_http_header=auth_header, auth_header=default_header)

        with open(infile, mode='rt', encoding='utf-8') as InFile, open(outfile, mode='wt',
                                                                       encoding='utf-8') as out_file:
            lazy_lines = (line.replace('\0', '') for line in InFile)
            records = csv.DictReader(lazy_lines, lineterminator=csvlt, delimiter=csvdel, quotechar=csvquo)
            writer = csv.DictWriter(out_file, fieldnames=['id', 'email', 'message'], lineterminator=csvlt,
                                    delimiter=csvdel, quotechar=csvquo)
            writer.writeheader()

            for record in records:
                api_call = False
                # if id is not provided create new record
                if record['id'] == '':
                    endpoint = 'rest/default/V1/customers/'
                    method = 'post'
                    api_call = True
                else:
                    api_call, endpoint, method = get_info(record['id'])
                if api_call:
                    ca = []
                    attr = {}
                    for key in record.keys():
                        if record[key] == '':
                            continue
                        # custom attributes
                        if key[:6] == 'custom':
                            ca.append({"attribute_code": key.replace('custom_', ''), 'value': record[key]})
                        # standard attributes
                        else:
                            attr[key] = record[key]
                    # insert customer attributes to all standard attributes
                    attr["custom_attributes"] = ca
                    payload = {'customer': attr}
                    if method == 'put':
                        result = cl_work.put_raw(endpoint, data=json.dumps(payload))
                    elif method == 'post':
                        result = cl_work.post_raw(endpoint, data=json.dumps(payload))
                    if result.ok:
                        continue
                    writer.writerow({'id': record['id'], 'email': record['email'],
                                     'message': json.loads(result.text)['message'].replace('"', '')})
                    output_log = True
                else:
                    writer.writerow({'id': record['id'], 'email': record['email'], 'message': 'id does not exist'})
                    output_log = True
        if output_log:
            logging.warning('something failed, check log table')
        else:
            logging.info('done, everything ok')

        self.write_manifest(out_table)


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
