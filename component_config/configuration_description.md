## Component configuration
Component has two main features
- Update customer attributes in Magento 2
- Create new customers

### Important parameters
1. URL
 - The URL of your Magento eCommerce Platform
 - Please enter your URL input before 'rest/V1/'
 - Example:
   - Full Link: https://magento-xxx.cloudwaysapps.com/admin/rest/default/V1
   - Input: https://magento-xxx.cloudwaysapps.com/
2. Username
 - Please ensure your account has the right privilege to access the defined endpoint

3. Password

### Structure of input file
- fields of data in input files must be named in accordance with API documentation [here](https://magento.redoc.ly/2.4.3-admin/tag/customerscustomerId#operation/customerCustomerRepositoryV1SavePut)
- it is possible to change all attributes except of **addresses** and **extension_attributes**
- in case you want to change **custom_attributes** use the prefix **custom_**, example:
  - custom attribute name = Atr1example
  - custom attribute value = 654
  - *file structure*:
>id,email, custom_Atr1example   
>1,test@emailcom, 654

  - If **id is not provided** (NULL value in input table), new records will be created (need to satisfy all required fields based on [documentation](https://magento.redoc.ly/2.4.3-admin/tag/customers) and your account setup)
  - If **id doesn't exist** in Magento or not all required fields are provided log message will be written into output table for problematic rows 



Detail Magento 2 Online Documentation & Endpoints can be found [here](https://devdocs.magento.com/guides/v2.4/rest/bk-rest.html)
